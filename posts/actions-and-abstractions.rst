.. title: Actions and Abstractions
.. slug: actions-and-abstractions
.. date: 2021-03-06 22:21:27 UTC+01:00
.. type: text
.. tags: hybrid automata
.. has_math: true

Here the |HA| from `Lazy Hybrid Automaton <../lazy-hybrid-automata>`_ is extended with actions, in the form of an algebraic equation system. Actions are then seen in action as we define and illustrate a method of creating piewise affine abstractions through simulation. 

.. TEASER_END
   
Actions in Theory
=================

Actions (also known as jumps) describe a computation to be made during a transition, from [Schaft2000]_:

  The transition from the discrete state :math:`l` to :math:`l^\prime` is *enabled* when the continuous state :math:`x` is in :math:`\text{Guard}_{ll\prime}`, while during the transition the continuous state :math:`x` jumps to a value :math:`x^\prime` given by the relation :math:`(x,x^\prime) \in \text{Jump}_{ll^\prime}`.

When simulating a |HA|, in the absence of actions, the final continuous state of the current continuous problem is the initial continuous state of the next:

.. graphviz::

   digraph{
     label="Simulation of a Hybrid Automaton"
     center=true
     node[style=filled;fillcolor="#f0c674";shape=box]
     sdp[label="Simulate Discrete\nProblem"]
     {
       rank="same"
       ccp[label="Construct Continuous\nProblem"]
       mte[label="Map to\nDiscrete Edge"]
     }
     scp[label="Simulate Continuous\nProblem"]
     sdp -> ccp [label="Halted\nLocation"]
     ccp -> scp [label="Continuous\nProblem"]
     scp -> mte [label="Active\nConstraint"]
     scp -> ccp [constraint=false;label="Final\nContinuous\nState"]
     mte -> sdp [label="Next\nEdge"]
   }

Actions explicitly transform these final continuous states to initial continuous states. Note that these initial states are still subject to the computation of consistent intitial conditions when constructing the continuous problem:

.. graphviz::

   digraph{
     label="Simulation of Hybrid Automaton with Actions"
     center=true
     node[style=filled;fillcolor="#f0c674";shape=box]
     sdp[label="Simulate Discrete\nProblem"]
     {
       rank="same"
       ccp[label="Construct Continuous\nProblem"]
       aoa[label="Apply action";fillcolor="#b5bd68"]
       mte[label="Map to\nDiscrete Edge"]
       ccp:e -> aoa:w [style=invis]
       aoa:e -> mte:w [style=invis]
     }
     scp[label="Simulate Continuous\nProblem"]
     sdp -> ccp [xlabel="Halted\nLocation"]
     ccp -> scp [label="Continuous\nProblem"]
     scp -> mte [label="Active\nConstraint"]
     scp -> aoa [label="Final\nContinuous\nState"]
     aoa -> ccp [constraint=false;label="Initial\nContinuous\nState"]
     mte -> sdp [label="Next\nEdge"]
   }
  
Unlike the definition of a |HA| in [Schaft2000]_ we do not bother with the invariant, which seem like a redundant feature whose functionality is covered by transitions and guards. We also pick the transition apart, even moreso than in `Structured Transition Guards <../structured-transition-guards>`_. Instead of using a 5-tuple of source, target, symbol, guard and action, we take a more decoupled approach.

We deconstruct the transition into a symbol-function, :math:`S \colon L \to A`, a target-function, :math:`T \colon L \times A \to L`, and a guard-function :math:`G \colon L \times A \to X \to \mathbb{B}`, where :math:`A` is the alphabet. These are all lifted up into the tuple defining the |HA|. The actions are also lifted up, and defined as a function mapping locations and symbols to algebraic equations:

.. math::

   H \colon L \times A \to X \times X \to X

Thus, given a location :math:`l`, we can compute the symbols of the edges :math:`S(l) = \{ s^\prime \dots \}`, as well as their corresponding targets :math:`T(l,s^\prime)`, guards :math:`G(l,s^\prime)`, and actions :math:`H(l,s^\prime)`. Given a final state of a continuous problem, :math:`X^-`, the initial state of the next continuous problem is here any :math:`X^+` that satisfies :math:`H(l,s^\prime)(X^-,X^+) = 0`. A |HA| with the action :math:`H(l,s^\prime)(X^-,X^+) = X^- - X^+`, is equivalent to a |HA| without an action, as it will be the case that :math:`X^+ = X^-`. 

We can now restate the definition of the |HA|:

.. math::
   
   H^M_N = (L,X,A,F,S,T,G,H)

And its components:
   
.. math::
   
   \begin{aligned}
   L &\subseteq \Z^M & S &\colon L \to A \\
   X &= \R^N \times \R^N \times \R & T &\colon L \times A \to L \\
   A &\subseteq \Z & G &\colon L \times A \to X \to \mathbb{B} \\
   F &\colon L \to X \to \R^N & H &\colon L \times A \to X \times X \to X
   \end{aligned}

An abstractor so primitive
==========================

We put actions to work by constructing, in a primitive fashion, abstractions through simulation. An abstraction, in the context of system-theory, is described in f.ex. [Pappas1996]_:

  Webster's dictionary defines the word abstraction as "the act or process of separating the inherent qualities or properties of something from the actual physical object or concept to which they belong". In system theory, the objects are usually dynamical or control systems, the properties are usually the behaviors of certain variables of interest and the act of separation is essentially the act of capturing all interesting behaviors. In summary, Webster's definition can be applied to define the abstraction of a system to be another system which captures all system behavior of interest. Behaviors of interest are captured by an abstracting map denoted by :math:`\alpha`. Abstracting maps are provided by the user depending on what information is of interest. 

And is illustrated in **Fig. 1 System Abstractions** which is reconstructed below:
  
.. graphviz::

   digraph{
     label="System Abstractions"
     node[shape=box]
     {
       rank=source
       sb[label="System\nbehaviours"]
       ab[label="Abstracted\nbehaviors"]
     }
     {
       rank=sink
       os[label="Original\nsystem"]
       as[label="Abstracted\nsystem"]
     }
     sb->ab[xlabel=<&alpha;>]
     os->as[label="        "]
     as->ab[constraint=false]
     os->sb[constraint=false]
   }

Our method of abstraction-through-simulation is parametrized on a concrete system, :math:`c`, a measure of badness, :math:`\beta`, and an interval :math:`T = [t_0,t_1]`. It constructs a piecewise affine abstraction, `a`, over :math:`T`, whose abstracted behaviour :math:`a(t)`, satisifies :math:`\left\lVert a(t) - c(t)\right\rVert_2 < \beta`, :math:`\forall t \in T`.

For a given conrete function, :math:`f` and constant :math:`\beta`, we start by defining the |HA|, :math:`H_{abstractor}(f,\beta)`, as:

.. math::

   \begin{aligned}
   L &= \{ 0 \} &
   S(l) &= \{ 0 \} &
   T(l,s) &= l\\
   F(l,s) &= \dot{x} &
   G(l,s) &= \left\lVert x - c(t) \right\rVert_2 - \beta &
   H(l,s) &= x^+ - c(t) 
   \end{aligned}

We then simulate this |HA| over :math:`T`, starting out at :math:`(0,c(t_0))`. Each self-transition marks the end of an interval, :math:`T_i = [t_{i},t_{i+1}]`, where :math:`\left\lVert a(t) - c(t) \right\rVert_2 < \beta`, :math:`\forall t \in T_i`. Note that this can hold outside :math:`T_i`, and that :math:`T_i` is a conservative approximation. The action, :math:`H`, will at each self-transition move the continous state of the |HA| to the current state of the concrete system, and ready itself to compute the next interval. The result of this simulation is a set of intervals :math:`\{T_0,\ldots,T_{n-1}\}`, or points in time :math:`\{t_0, \ldots, t_n\}`, which let us define the abstracted system, a |PWA|:

.. math::

   \begin{aligned}
   L &= \{ l_i \mid i \in n \} &
   S(l) &= \begin{cases} l \neq n \to \{ 0 \} \\ l = n \to \emptyset\end{cases} &
   T(l,s) &= l + 1\\
   F(l,s) &= \dot{x} - \frac{c(t_{l+1}) - c(t_{l})}{t_{l+1} - t_{l}} & & &
   G(l,s) &= t_{l + 1} - t
   \end{aligned}

We illustrate the method for :math:`f(t) = \sin({\pi \cdot t})`, :math:`\beta \in \{ 0.25, 0.5, 0.75 \}`, and :math:`T = [0,5]`. The simulations are done with hysj_ and sundials_, and illustrated with matplotlib\ [1]_. The abstraction functions are labelled :math:`a_{\beta}`, and the concrete function is labelled :math:`c`. First :math:`H_{abstraction}(f,\beta)` is simulated over :math:`T` to construct :math:`\{t_0, \ldots, t_n\}`:

.. raw:: html
      
   <video width="640" height="480" class="embed" controls>
         <source src="/files/actions-and-abstractions-0.mp4" type="video/mp4">
   </video>

We then compute the |PWA| abstractions, and finally simulate them over the same interval:

.. raw:: html
      
   <video width="640" height="480" class="embed" controls>
         <source src="/files/actions-and-abstractions-1.mp4" type="video/mp4">
   </video>
   
.. [1] Code listing :ref:`below <code>`.
       
Glossary
========

.. glossary::

   PWA
     Piecewise affine

   HA
     Hybrid automaton
     
.. |PWA| replace:: :term:`PWA`
.. |HA| replace:: :term:`HA`

Bibliography
============

.. [Pappas1996] "George J. Pappas and Shankar Sastry", "Towards Continuous Abstractiosn of Dynamical and Control Systems"
.. [Schaft2000] "Arjan van der Schaft and Hans Schumacher", "An introduction to hybrid dynamical systems"

.. Links

.. _hysj: https://gitlab.com/jehelset/hysj
.. _sundials: https://computing.llnl.gov/projects/sundials
.. _matplotlib: https://matplotlib.org/

.. _code: 
              
Code
====

.. include:: code/actions-and-abstractions.py
   :code: python
