import hysj
import numpy 
from matplotlib import pyplot as plot

def make_problem(A,b):
  import math
  
  continuous = hysj.simulators.continuous
  
  order = 1
  N = len(A)

  ir = hysj.mathematics.Program()
  t = ir.symbol()

  X = [ ir.variable(t,order,i) for i in range(N) ]
  F = [ ir.sine(ir.multiplication([ir.constant(a),t])) for a in A ]
  E = [ ir.equality(x[-1],f) for x,f in zip(X,F) ]
  R = [ ir.strict_inequality(ir.constant(b),x[0]) for x in X ]
  G = [ ir.conjunction([ir.assertion(r) for r in R]) ]
  
  t_begin  = 0.0
  t_size   = 5
  t_end    = t_size * math.pi
  t_count  = t_size * 256
  t_delta  = (t_end - t_begin) / (t_count - 1)
  
  t_space = numpy.linspace(t_begin,t_end,num = t_count)

  problem = continuous.Problem(program = ir,
                               variables    = continuous.Variables(independent = t,
                                                                   dependent   = X),
                               equations    = E,
                               constraints  = G)
  
  X_init = [ (order+1)*[0] for i in range(N) ] 
  initial_valuation = continuous.Valuation(independent = t_begin,
                                           dependent   = X_init)
  
  config = continuous.Config(tolerance = continuous.Tolerance(relative = 1.0e-4,
                                                              absolute = 1.0e-3),
                             step = continuous.Step(mode  = continuous.StepMode.fixed,
                                                    delta = t_delta),
                             stop = t_end)
  
  return problem,initial_valuation,config

def simulate(A,b):
  from copy import deepcopy
  
  continuous = hysj.simulators.continuous
  
  problem,initial_valuation,config = make_problem(A,b)
  
  solution = continuous.make_solution(problem           = problem,
                                      initial_valuation = initial_valuation,
                                      config            = config)

  states = []
  print('simulating...')
  while event := solution():
    state = solution.state()
    if event in [continuous.Event.init,
                 continuous.Event.step,
                 continuous.Event.stop]:
      states.append(deepcopy(state))
    if event == continuous.Event.stop:
      break
    if event == continuous.Event.fail:
      raise Exception('simulation failed')
  print('done...')
  dfa = solution.constraints().graphs[0]
  return states,dfa

def make_dfa_pos(dfa,xy,wh):
  import networkx

  box_f = 3 / 4
  wh = [ e * box_f for e in wh ]
  xy = [ xy[i] + wh[i] * (1 - box_f)/2 for i in range(len(xy)) ]
  
  nx = networkx.DiGraph()
  for v in dfa.vertices():
    nx.add_node(v.value)

  nx_edges = []
  for e in dfa.edges():
    v_in,v_out = dfa(e).ports
    nx.add_edge(v_in.value,v_out.value)
    nx_edges.append((v_in.value,v_out.value))

  nx_pos = list(networkx.drawing.nx_agraph.graphviz_layout(nx).values())
  nx_xmin = min([x for x,y in nx_pos])
  nx_xmax = max([x for x,y in nx_pos])
  nx_ymin = min([y for x,y in nx_pos])
  nx_ymax = max([y for x,y in nx_pos])
  nx_xext = nx_xmax - nx_xmin
  nx_yext = nx_ymax - nx_ymin

  def mapx(x):
    return ((x - nx_xmin) / nx_xext) * wh[0] + xy[0]
  def mapy(y):
    return ((y - nx_ymin) / nx_yext) * wh[1] + xy[1]

  return [ (mapx(x),mapy(y)) for x,y in nx_pos ]


dfa_vertex_radius = 0.04

def make_dfa_vertex_patches(dfa,pos):
  from matplotlib.patches import Circle
  return [ Circle([x,y],
                  radius = dfa_vertex_radius,
                  fill = True,
                  linewidth = 1.0,
                  facecolor = 'white',
                  edgecolor = 'black') for x,y in pos ]

def make_dfa_edge_patches(dfa,pos):
  from matplotlib.patches import FancyArrowPatch
  patches = []
  for e in dfa.edges():
    v_in,v_out = dfa(e).ports
    if v_in == v_out:
      continue
    
    r = dfa_vertex_radius / 2.0
    a = numpy.array(pos[v_in.value])
    b = numpy.array(pos[v_out.value])

    c = b - a
    n = c / numpy.linalg.norm(c)
    b = b - n * (1.1*r)
    patch = FancyArrowPatch(posA = a,
                            posB = b,
                            linewidth = 1.0,
                            arrowstyle='-|>,head_length=4.0,head_width=3.0',
                            edgecolor = 'black',
                            linestyle = (0,(1,3)),
                            facecolor = 'black',
                            connectionstyle='arc3,rad=0.5')
      
    patches.append(patch)
  return patches

def make_dfa_patches(dfa,xy,wh):
  pos = make_dfa_pos(dfa,xy,wh)
  return make_dfa_vertex_patches(dfa,pos),make_dfa_edge_patches(dfa,pos)

def animate(A,b,states,dfa):
  print('animating...')
  from matplotlib.animation import FuncAnimation as Animation
  from matplotlib.animation import writers as animation_writers
  N = len(A)
  
  xdata = [ [s.valuation.dependent[i][1] for s in states ] for i in range(N) ]
  ydata = [ [s.valuation.dependent[i][0] for s in states ] for i in range(N) ]
  vdata = [ s.constraints[0] if s.constraints else None for s in states ] 
  

  figure,axes = plot.subplots(1,1)

  axes.set_aspect("equal", adjustable="datalim")  
  axes.set_xlabel('$x_i$')
  axes.set_ylabel('$\dot{x_i}$')
  

  xmin,xmax = min([x for X in xdata for x in X]),max([x for X in xdata for x in X])
  ymin,ymax = min([y for Y in ydata for y in Y]),max([y for Y in ydata for y in Y])
  xext = xmax - xmin
  yext = ymax - ymin

  axes_margin = 0.1

  axes.set_xlim([xmin - axes_margin,xmax + axes_margin])
  axes.set_ylim([ymin - axes_margin,ymax + axes_margin])

  dfa_vertex_patches,dfa_edge_patches = make_dfa_patches(dfa,[xmin,ymin],[xext,yext])
  for p in dfa_edge_patches + dfa_vertex_patches:
    axes.add_patch(p)
    
  lines  = [ axes.plot([],[],label=f'$x_{{{i}}}$')[0] for i in range(N) ]
  points = [ axes.plot([],[],color=lines[i].get_color(),marker='o')[0] for i in range(N) ]
    
  axes.axhline(y = b,color='red',label='b')

  axes.legend(bbox_to_anchor=(0,1.02,1,0.2), loc="lower center",
              borderaxespad=0, ncol=(N + 1))

  frame_step   = 2
  frame_count  = len(states) // frame_step
  frame_window = frame_step * 10
    
  def draw_X(first,last):
    for i in range(N):
      lines[i].set_data(xdata[i][first:last],ydata[i][first:last])
      points[i].set_data(xdata[i][last:last+1],ydata[i][last:last+1])
    
  def draw_dfa(last):

    def vertex(i):
      if i < 0 or i >= len(states):
        return None
      return vdata[i]

    def leave(v):
      dfa_vertex_patches[v.value].set_facecolor('white')

    def enter(v):
      dfa_vertex_patches[v.value].set_facecolor('green' if dfa(v)() else 'red')
    
    v_prev = vertex(last - frame_step)
    v_next = vertex(last)
    if v_prev:
      leave(v_prev)
    enter(v_next)
  
  def animate(frame_index):
    last  = min((frame_index + 1) * frame_step,len(states) - 1)
    first = max(0,last - frame_window * frame_step)

    draw_X(first,last)
    draw_dfa(last)
    return lines + points
  
  animation = Animation(figure,
                        animate,
                        frames = frame_count,
                        interval = 1,
                        blit = True)

  animation.save(f'structured-transition-guards-{N}.mp4',
                 writer = animation_writers['ffmpeg'](fps = 30),
                 dpi = 100.0)
  print('done')

def make_animation(b,A):
  states,dfa = simulate(A,b)
  animate(A,b,states,dfa)
  
try:
  make_animation(A = [1.0          ],b = 0.5)
  make_animation(A = [0.75,1.25    ],b = 0.5)
  make_animation(A = [0.75,1.0,1.25],b = 0.5)
except Exception as e:
  print(str(e))

