# -*- coding: utf-8 -*-

# Copyright © 2012-2021 Roberto Alsina and others.

# Permission is hereby granted, free of charge, to any
# person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the
# Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the
# Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice
# shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import datetime
import re

from docutils import languages, nodes, utils
from docutils.parsers.rst import Directive, directives, roles
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils.parsers.rst.directives.body import MathBlock
from docutils.transforms import Transform

from nikola.plugin_categories import RestExtension
from nikola.plugins.compile.rest import add_node
from nikola.utils import get_logger

logger = get_logger("rest_sphinx_roles")

explicit_title_re = re.compile(r"^(.+?)\s*(?<!\x00)<(.*?)>$", re.DOTALL)

def split_explicit_title(text):
  """Split role content into title and target, if given."""
  match = explicit_title_re.match(text)
  if match:
    return True, match.group(1), match.group(2)
  return False, text, text

_litvar_re = re.compile("{([^}]+)}")

def emph_literal_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
  text = utils.unescape(text)
  pos = 0
  retnode = nodes.literal(role=typ.lower(), classes=[typ])
  for m in _litvar_re.finditer(text):
    if m.start() > pos:
      txt = text[pos:m.start()]
      retnode += nodes.Text(txt, txt)
      retnode += nodes.emphasis(m.group(1), m.group(1))
      pos = m.end()
    if pos < len(text):
      retnode += nodes.Text(text[pos:], text[pos:])
  return [retnode], []

def make_link_role(base_url, prefix):
  def role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
    text = utils.unescape(text)
    has_explicit_title, title, part = split_explicit_title(text)
    try:
      full_url = base_url % part
    except (TypeError, ValueError):
      inliner.reporter.warning(
        "unable to expand %s extlink with base URL %r, please make "
        "sure the base contains '%%s' exactly once" % (typ, base_url),
        line=lineno,
      )
      full_url = base_url + part
      if not has_explicit_title:
        if prefix is None:
          title = full_url
        else:
          title = prefix + part
          pnode = nodes.reference(title, title, internal=False, refuri=full_url)
      return [pnode], []
    return role

def set_source_info(directive, node):
  node.source, node.line = directive.state_machine.get_source_and_line(directive.lineno)

class Glossary(Directive):
  has_content = True

  def run(self):
    node = nodes.Element()
    node.document = self.state.document
    self.state.nested_parse(self.content, self.content_offset, node)
    node[0]["classes"] = ["glossary", "docutils"]
    # Set correct IDs for terms
    for term in node[0]:
      if type(term[0]) == str:
        logger.info(term[0])
        continue
      new_id = "term-" + nodes.make_id(term[0].astext())
      term[0]["ids"].append(new_id)
    return [node[0]]

def term_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
  text = utils.unescape(text)
  target = "#term-" + nodes.make_id(text)
  pnode = nodes.reference(text, text, internal=True, refuri=target)
  pnode["classes"] = ["reference"]
  return [pnode], []

def eq_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
  # FIXME add stylable span inside link
  text = utils.unescape(text)
  target = "#eq-" + nodes.make_id(text)
  pnode = nodes.reference(text, text, internal=True, refuri=target)
  pnode["classes"] = ["reference"]
  return [pnode], []

_ref_re = re.compile("^(.*)<(.*)>$")

class RefVisitor(nodes.NodeVisitor, object):
  text = None

  def __init__(self, document, label):
    self._label = label
    super().__init__(document)

  def visit_target(self, node):
    if self._label not in node.attributes["ids"]:
      return
    sibs = node.parent.children
    if sibs.index(node) + 1 >= len(sibs):
      return
    next_sib = sibs[sibs.index(node) + 1]
    if isinstance(next_sib, nodes.figure): # text has to be the figure caption
      self.text = [x for x in next_sib.children if isinstance(x, nodes.caption)][0].astext()
    elif isinstance(next_sib, nodes.section): # text has to be the title
      self.text = next_sib.attributes["names"][0].title()

  def unknown_visit(self, node):
    pass

def ref_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
  """Reimplementation of Sphinx's ref role, but just links unconditionally."""

  msg_list = []
  match = _ref_re.match(text)
  if match is not None:
    text = match.groups()[0].strip()
    target = "#" + match.groups()[1]
    pnode = nodes.reference(text, text, internal=True, refuri=target)
    return [pnode], msg_list

  visitor = RefVisitor(inliner.document, text)
  inliner.document.walk(visitor)
  if visitor.text is None:
    visitor.text = text.replace("-", " ").title()
    logger.info(
      f'ref label {text} is missing or not known yet at this point in the document or not immediately before figure or section. Proceeding anyway but title "{visitor.text}" in this ref may be wrong.')
    target = "#" + text
    pnode = nodes.reference(text, visitor.text, internal=True, refuri=target)
    pnode["classes"] = ["reference"]
  return [pnode], msg_list

class Abbreviation(nodes.Inline, nodes.TextElement):
  """Node for abbreviations with explanations."""
  
  def visit_abbreviation(self, node):
    attrs = {}
    if node.hasattr("explanation"):
      attrs["title"] = node["explanation"]
      self.body.append(self.starttag(node, "abbr", "", **attrs))

  def depart_abbreviation(self, node):
    self.body.append("</abbr>")

_abbr_re = re.compile("\((.*)\)$", re.S)
def abbr_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
  text = utils.unescape(text)
  m = _abbr_re.search(text)
  if m is None:
    return [Abbreviation(text, text)], []
  abbr = text[: m.start()].strip()
  expl = m.group(1)
  return [Abbreviation(abbr, abbr, explanation=expl)], []



math_option_spec = MathBlock.option_spec
math_option_spec["label"] = str

class Math(MathBlock):
  option_spec = math_option_spec
  def run(self):
    output = super().run()
    if "label" in self.options and output:
      new_id = "eq-" + self.options["label"]
      output[0]["ids"].append(new_id)
    return output

class Plugin(RestExtension):

  name = "rest_sphinx_roles"

  def set_site(self, site):
    self.site = site
    
    roles.register_local_role("term", term_role)
    roles.register_local_role("ref", ref_role)
    roles.register_local_role("eq", eq_role)
    roles.register_local_role("abbr", abbr_role)
    
    add_node(Abbreviation,
             Abbreviation.visit_abbreviation,
             Abbreviation.depart_abbreviation)

    directives.register_directive("glossary", Glossary)
    directives.register_directive("math", Math)    

    return super().set_site(site)

