# -*- coding: utf-8 -*-
import time

BLOG_AUTHOR = 'John Eivind Helset'
BLOG_TITLE = '0°'
SITE_URL = 'https://jehelset.gitlab.io/'
BLOG_EMAIL = 'jehelset@gmail.com'
BLOG_DESCRIPTION = 'Personal blog.'
DEFAULT_LANG = 'en'

NAVIGATION_LINKS = {
  DEFAULT_LANG: (
    ("/pages/notation", "Notation"),
    ("/archive.html", "Archives"),
    ("/categories/index.html", "Tags"),
    ("https://gitlab.com/jehelset/jehelset.gitlab.io","Repository"),
  ),
}

THEME = 'blackfur'
THEME_COLOR = '#5670d4'

PAGES = (
  ('pages/*.rst', 'pages', 'page.tmpl'),
)

POSTS = (
  ('posts/*.rst', 'posts', 'post.tmpl'),
)

TIMEZONE = 'Europe/Oslo'
COMPILERS = {
  'rest': ['.rst']
}

HIDDEN_TAGS = ['mathjax']

REDIRECTIONS = []

OUTPUT_FOLDER = 'public'
CACHE_FOLDER = 'cache'
FILES_FOLDERS = {'well-known': '.well-known',
                 'files': 'files'}

CODE_COLOR_SCHEME = 'default' 

INDEX_TEASERS = True
INDEX_READ_MORE_LINK = '<p class="more"><a href="{link}">{read_more}…</a></p>'
FEED_READ_MORE_LINK = '<p><a href="{link}">{read_more}…</a> ({min_remaining_read})</p>'
FEED_LINKS_APPEND_QUERY = False

LICENSE = """
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License BY-SA"
style="border-width:0; margin-bottom:12px;"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"></a>"""

CONTENT_FOOTER = 'Contents &copy; {date} <a href="mailto:{email}">{author}</a>'

CONTENT_FOOTER_FORMATS = {
  DEFAULT_LANG: (
    (),
    {
      'email': BLOG_EMAIL,
      'author': BLOG_AUTHOR,
      'date': time.gmtime().tm_year,
      'license': LICENSE
    }
  )
}

COMMENT_SYSTEM = ''
COMMENT_SYSTEM_ID = ''

STRIP_INDEXES = True
PRETTY_URLS = True
USE_BUNDLES = True
USE_CDN = False
USE_CDN_WARNING = True
FILE_METADATA_UNSLUGIFY_TITLES = True

USE_KATEX = True
SHOW_SOURCELINK = False
COPY_SOURCES = False
GENERATE_RSS = False

USE_TAG_METADATA = False
WARN_ABOUT_TAG_METADATA = False

FUTURE_IS_NOW = True

GLOBAL_CONTEXT = {}
GLOBAL_CONTEXT_FILLER = []

EXTRA_PLUGINS_DIRS = ['plugins']
