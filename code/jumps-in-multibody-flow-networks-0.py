from copy import deepcopy

import matplotlib.pyplot as plot

import hysj

continuous = hysj.solvers.continuous
discrete   = hysj.solvers.discrete
hybrid     = hysj.solvers.hybrid

def make_problem(A_value):
  
  ir = hysj.Instructions()

  t   = ir.symbol()

  order = 1

  A = [ ir.constant(a) for a in A_value ]
  X = ir.variable(t,order)

  Z = ir.zero()

  F = ir.equate(ir.subtract([A[0],X[1]]),Z)
  G = ir.assertion(ir.strictly_inequate(ir.subtract([A[1],X[0]]),Z))
  H = [([0,0],A[2])]

  return hybrid.Problem(
    instructions = ir,
    discrete     = discrete.Problem(make_symbols = lambda l: [0],
                                    make_target  = lambda l,s: (),
                                    is_immediate = lambda l,s: False),
    continuous   = hybrid.ContinuousProblem(variables = continuous.Variables(independent = t,
                                                                             dependent   = [X]),
                                            make_equations  = lambda m,l:   [F],
                                            make_constraint = lambda m,l,s: G),
    make_action  = lambda i,l,s: H)

def simulate(A,X_init):
  problem = make_problem(A)

  t_begin  = 0.0
  t_end    = 3.0
  t_count  = 200 + 1
  t_delta  = (t_end - t_begin) / (t_count - 1)
  
  initial_valuation = hybrid.Valuation(discrete   = [0],
                                       continuous = continuous.Valuation(independent = t_begin,
                                                                         dependent   = [X_init]))

  config = hybrid.Config(
    discrete   = discrete.Config(),
    continuous = continuous.Config(
      tolerance = continuous.Tolerance(relative = 1.0e-2,
                                       absolute = 1.0e-3),
      step = continuous.Step(
        mode  = continuous.StepMode.fixed,
        delta = t_delta),
      stop = t_end))
  
  solution = hybrid.make_solution(problem       = problem,
                                  initial_valuation = initial_valuation,
                                  config        = config)

  trajectories = []
  print('simulating...')
  while event := solution():
    if(event == discrete.Event.vertex):
      trajectories.append([])
    if(event == continuous.Event.step or
       event == continuous.Event.init or
       event == continuous.Event.root):
      trajectories[-1].append(deepcopy(solution.continuous().state().valuation))
    if(event == continuous.Event.stop):
      break
  print('done...')
  return trajectories

def illustrate(A,i):

  trajectories = simulate(A = A,X_init = [0.0,A[0]])
  figure, axis = plot.subplots()

  axis.set_title(f'A = {A}')
  axis.set_ylabel('x')
  axis.set_xlabel('t')

  axis.axhline(y = A[1],color='#17DCD4',label='a1')
  axis.axhline(y = A[2],color='#DC8317',label='a2')

  last = None
  for j,trajectory in enumerate(trajectories):
    x_axis = [ s.independent for s in trajectory ]
    y_axis = [ s.dependent[0][0] for s in trajectory ]
    if j == 0:
      axis.plot(x_axis,y_axis,c = '#dc1717',label = 'x')
    else:
      axis.plot(x_axis,y_axis,c = '#dc1717')

    if not last is None:
      last_x,last_y = last
      axis.plot([last_x,x_axis[0]],[last_y,y_axis[0]],c = '#dc1717',ls = '--',markevery=[0,1],marker='o')
    last = (x_axis[-1],y_axis[-1])
  axis.legend(loc='upper left')
  figure.tight_layout()
  figure.savefig(f'files/jumps-in-multibody-flow-networks-0-{i}.svg')

try:
  illustrate([1.0,1.0,0.0],0)
  illustrate([2.0,1.0,0.5],1)
  illustrate([0.1,1.0,0.0],2)
except Exception as e:
  print(f'exception: {str(e)}')
