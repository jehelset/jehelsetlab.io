import hysj

continuous = hysj.simulators.continuous
discrete   = hysj.simulators.discrete
hybrid     = hysj.simulators.hybrid

def make_concrete_problem(abstraction_badness_limit_constants):
  print('modelling...')
  number_of_abstractions = len(abstraction_badness_limit_constants)
  abstractions = range(number_of_abstractions)
  
  #NOTE: discrete model - jeh
  bad_abstraction_symbols = [ i for i in abstractions ]

  #NOTE: continuous model - jeh
  ir = hysj.mathematics.Program()
  
  t = ir.symbol()
  pi = ir.pi()
  
  concrete = ir.sine(ir.multiplication([pi,t]))
  abstract_variables = [ ir.variable(t,1) for i in abstractions ]
  
  abstraction_badness = [ ir.power(ir.subtraction([concrete,abstract_variables[i][0]]),ir.constant(2))
                          for i in abstractions ]
  abstraction_badness_limits = [ ir.constant(l) for l in  abstraction_badness_limit_constants ]
  
  abstraction_is_bad = [
    ir.assertion(
      ir.strict_inequality(
        abstraction_badness_limits[i],
        abstraction_badness[i]))
    for i in abstractions ]

  equations = [ ir.equality(abstract_variable[1],ir.zero())
                for abstract_variable in abstract_variables ]
  
  problem = hybrid.Problem(
    program = ir,
    discrete = discrete.Problem(
      make_symbols = lambda l: bad_abstraction_symbols,
      make_target  = lambda l,s: l, 
      is_immediate = lambda l,s: False),
    continuous = hybrid.ContinuousProblem(
      variables = continuous.Variables(independent = t,
                                       dependent   = abstract_variables),
      make_equations  = lambda m,l: equations,
      make_constraint = lambda m,l,s: abstraction_is_bad[s]),
    make_action = lambda m,l,s: [([s,0],concrete)])

  problem.time = t
  problem.concrete = concrete
  problem.abstractions = abstractions
  problem.abstract_variables = abstract_variables
  problem.abstraction_badness = abstraction_badness
  problem.abstraction_badness_limits = abstraction_badness_limits

  problem.initial_valuation = hybrid.Valuation(
    discrete = [0],
    continuous = continuous.Valuation(
      independent = 0.0,
      dependent = [[0.0,0.0] for i in problem.abstractions]))

  problem.config = hybrid.Config(
    discrete   = discrete.Config(),
    continuous = continuous.Config(
      tolerance = continuous.Tolerance(
        relative = 1.0e-2,
        absolute = 1.0e-3),
      step = continuous.Step(
        mode  = continuous.StepMode.fixed,
        delta = 0.001),
      stop = 5.01)) #FIXME: need to check if tstop reached when overlapping with constraint - jeh
  
  print('done...')
  return problem

def simulate_concrete_problem(problem):
  from copy import deepcopy
  print('simulating...')
  
  solution = hybrid.make_solution(
    problem           = problem,
    initial_valuation = problem.initial_valuation,
    config            = problem.config)

  trajectories = []

  events = []
  while event := solution():
    if(event == discrete.Event.vertex):
      trajectories.append((
        tuple(solution.discrete().state().location()),
        []))
    if(event == continuous.Event.start or
       event == continuous.Event.step):
      trajectories[-1][-1].append(deepcopy(solution.continuous().state().valuation))
    if(event == continuous.Event.stop or
       event == continuous.Event.fail):
      break
    events.append(event)
  print('done...')
  return events,trajectories

def make_abstract_problem(concrete_problem,concrete_trajectories):
  import itertools

  ir = hysj.mathematics.Program()

  T = [ s.independent for t in concrete_trajectories for s in t[1] ] 
  P = [ [ s.dependent[i][0] for t in concrete_trajectories for s in t[1] ] for i in concrete_problem.abstractions ]

  I = [ [ next(g)
          for i,g in itertools.groupby(range(len(T)),lambda j:P[i][j]) ]
        for i in concrete_problem.abstractions ]
  A = [ [ir.constant((P[i][I[i][j+1]] - P[i][I[i][j]])/(T[I[i][j+1]] - T[I[i][j]]))
         for j in range(len(I[i]) - 1) ]
        for i in concrete_problem.abstractions ]

  t = ir.symbol()
  zero = ir.zero()
    
  X = [ ir.variable(t,1)
        for i in concrete_problem.abstractions ]
  F = [ [ ir.equality(ir.subtraction([a,X[i][1]]),zero)
          for a in A[i] ]
        for i in concrete_problem.abstractions ]

  T_next = [ [ T[I[i][j + 1]] 
               for j in range(len(I[i]) - 1) ]
             for i in concrete_problem.abstractions ]
  G = [ [ ir.assertion(ir.strict_inequality(ir.constant(T_next[i][j]),t))
          for j in range(len(I[i]) - 1) ]
        for i in concrete_problem.abstractions ]

  def make_equations_fn(m,l):
    return [F[i][j] for i,j in enumerate(l)]
  def make_constraint_fn(m,l,s):
    return G[s][l[s]]
  
  abstract = hybrid.Problem(
    program = ir,
    discrete = discrete.Problem(
      make_symbols = lambda l: [ i for i in concrete_problem.abstractions if l[i] != (len(I[i]) - 2)],
      make_target  = lambda l,s: l[:s] + [l[s]+1] + l[s+1:],
      is_immediate = lambda l,s: False),
    continuous = hybrid.ContinuousProblem(
      variables = continuous.Variables(independent = t,
                                       dependent   = X),
      make_equations = make_equations_fn,
      make_constraint = make_constraint_fn))

  abstract.initial_valuation = hybrid.Valuation(
    discrete = [ 0 for i in concrete_problem.abstractions ],
    continuous = continuous.Valuation(
      independent = 0.0,
      dependent = [[0.0,0.0] for i in concrete_problem.abstractions ]))
  return abstract

def simulate_abstract_problem(concrete_problem,abstract_problem):
  from copy import deepcopy
  print('simulating...')
  solution = hybrid.make_solution(
    problem           = abstract_problem,
    initial_valuation = abstract_problem.initial_valuation,
    config            = concrete_problem.config)

  trajectories = []
  events = []
  while event := solution():
    if(event == discrete.Event.vertex):
      trajectories.append((
        tuple(solution.discrete().state().location()),
        []))
    if(event == continuous.Event.start or
       event == continuous.Event.step):
      trajectories[-1][-1].append(deepcopy(solution.continuous().state().valuation))
    if(event == continuous.Event.stop or
       event == continuous.Event.fail):
      break
    events.append(event)
  print('done...')
  return events,trajectories

def make_concrete_series(concrete_problem,
                         trajectories):
  calculator = hysj.calculators.Basic(concrete_problem.program)
  time = [ s.independent for t in trajectories for s in t[1] ]
  return [ calculator([concrete_problem.concrete],[concrete_problem.time],[t]) for t in time ]
  
def make_abstraction_badness_limits(concrete_problem):
  calculator = hysj.calculators.Basic(concrete_problem.program)
  return [ calculator(l) for l in
           concrete_problem.abstraction_badness_limits ]


def illustrate_concrete_simulation(problem,trajectories):
  print('illustrating...')
  import hysj.latex
  from matplotlib import pyplot as plot
  from matplotlib.animation import FuncAnimation as Animation
  from matplotlib.animation import writers as animation_writers

  variables = problem.continuous.variables
  
  def latex_symbol_renderer(s,d):
    return {
      tuple(variables.independent.index()): 't'
    }.get(tuple(s.index()),f'a_{d}')
  
  latex_renderer = hysj.latex.Renderer(problem.program,latex_symbol_renderer)  

  time = [ s.independent for t in trajectories for s in t[1] ]
  
  abstract_series = [ [ s.dependent[i][0] for t in trajectories for s in t[1] ] for i in problem.abstractions ]

  number_of_series = 2*len(abstract_series) + 1 
  colormap = plot.get_cmap('Dark2')

  concrete_color = colormap(len(problem.abstractions))
  abstraction_colors = [ colormap(i) for i in problem.abstractions ]

  concrete_series = make_concrete_series(problem,trajectories)

  calculator = hysj.calculators.Basic(problem.program)
  def make_abstraction_badness_series():
    return [ [ calculator([problem.abstraction_badness[i]],
                          [problem.time,problem.abstract_variables[i][0]],
                          [t,a])
             for t,a in zip(time,abstract_series[i]) ]
             for i in problem.abstractions ]

  abstraction_badness_series = make_abstraction_badness_series()

  abstraction_badness_colors = [ colormap(i + len(problem.abstractions) + 1) for i in problem.abstractions ]


  series = [ [concrete_series,*abstract_series],
             [*abstraction_badness_series] ]

  frame_step   = 5
  frame_count  = len(time) // frame_step

  figure, axes = plot.subplots(2,1,sharex=True,gridspec_kw={'height_ratios': [3,1]})

  margin = 0.1
  axes[0].set_xlim((time[0],time[-1]))
  axes[0].set_ylim((-1.0-margin,1.0+margin))

  abstraction_badness_limits = make_abstraction_badness_limits(problem)
  
  axes[1].set_ylim(-margin,max(abstraction_badness_limits) + margin)
  axes[1].set_xlabel(f'${latex_renderer(variables.independent)}$')

  for i in problem.abstractions:
    axes[1].axhline(y = abstraction_badness_limits[i],
                    color = abstraction_colors[i],
                    linestyle='--')

  labels = [ [ 'c', *[f'$a_{{{l}}}$' for l in abstraction_badness_limits ] ],
             [f'$a^{{\ast}}_{{{l}}}$' for l in abstraction_badness_limits ] ]
  styles = [ [ '-', *['-' for i in problem.abstractions] ],
             ['-' for i in problem.abstractions]]
  colors = [ [concrete_color, *abstraction_colors],
             abstraction_colors ]
  lines = [ [ axes[i].plot([],[],color=colors[i][j],linestyle=styles[i][j],label=labels[i][j])[0]
              for j,s in enumerate(S) ]
            for i,S in enumerate(series) ]

  axes[0].title.set_text(f'abstracting ${latex_renderer(problem.concrete)}$')
  axes[1].title.set_text('abstraction badness')

  axes[0].legend(loc='lower left')
  
  def animate(frame_index):
    first, last = 0, min((frame_index + 1) * frame_step,len(time) - 1)

    for i,S in enumerate(series):
      for j,s in enumerate(S):
        lines[i][j].set_data(time[first:last],series[i][j][first:last])
    for a in axes:
      a.relim()
      a.autoscale_view()

      return [ l for L in lines for l in L ]
  
  animation = Animation(figure,
                        animate,
                        frames = frame_count,
                        interval = 1,
                        blit = True)

  animation.save('files/actions-and-abstractions-0.mp4',
                 writer = animation_writers['ffmpeg'](fps=30),
                 dpi = 150.0)
  print('done...')
  
def illustrate_abstract_simulation(concrete_problem,abstract_problem,
                                   concrete_trajectories,abstract_trajectories):
  print('illustrating...')
  import hysj.latex
  from matplotlib import pyplot as plot
  from matplotlib.animation import FuncAnimation as Animation
  from matplotlib.animation import writers as animation_writers

  variables = abstract_problem.continuous.variables

  abstractions = range(len(variables.dependent))
  
  def latex_symbol_renderer(s,d):
    return {
      tuple(variables.independent.index()): 't'
    }.get(tuple(s.index()),f'a_{d}')
  
  latex_renderer = hysj.latex.Renderer(abstract_problem.program,latex_symbol_renderer)  

  time = [ s.independent for t in abstract_trajectories for s in t[1] ]
  series = [ make_concrete_series(concrete_problem,abstract_trajectories),
             *[ [ s.dependent[i][0] for t in abstract_trajectories for s in t[1] ] for i in abstractions ] ]

  colormap = plot.get_cmap('Dark2')

  concrete_color = colormap(len(concrete_problem.abstractions))
  abstraction_colors = [ colormap(i) for i in concrete_problem.abstractions ]

  colors = [ concrete_color, *abstraction_colors ]

  calculator = hysj.calculators.Basic(abstract_problem.program)
  
  frame_step   = 5
  frame_count  = len(time) // frame_step

  figure, axes = plot.subplots(1,1)


  margin = 0.1
  axes.set_xlim((time[0],time[-1]))
  axes.set_ylim((-1.0-margin,1.0+margin))


  abstraction_badness_limits = make_abstraction_badness_limits(concrete_problem)

  labels = [ 'c', *[f'$a_{{{l}}}$' for l in abstraction_badness_limits ] ]
  linestyles = [ '--', *['-' for l in abstraction_badness_limits ] ]
  lines = [ axes.plot([],[],color=colors[i],label=labels[i],linestyle=linestyles[i])[0]
            for i in range(len(series)) ]
  
  axes.title.set_text(f'abstractions')
  axes.legend(loc='lower left')
  
  def animate(frame_index):
    first, last = 0, min((frame_index + 1) * frame_step,len(time) - 1)

    for i,S in enumerate(series):
      lines[i].set_data(time[first:last],
                        series[i][first:last])
    axes.relim()
    axes.autoscale_view()

    return lines
  
  animation = Animation(figure,
                        animate,
                        frames = frame_count,
                        interval = 1,
                        blit = True)

  animation.save('files/actions-and-abstractions-1.mp4',
                 writer = animation_writers['ffmpeg'](fps=30),
                 dpi = 150.0)
  print('done...')

try:
  concrete_problem = make_concrete_problem([0.25,0.5,0.75])
  _,concrete_trajectories = simulate_concrete_problem(concrete_problem)
  abstract_problem = make_abstract_problem(concrete_problem,concrete_trajectories)
  _,abstract_trajectories = simulate_abstract_problem(concrete_problem,abstract_problem)

  illustrate_concrete_simulation(concrete_problem,concrete_trajectories)
  illustrate_abstract_simulation(concrete_problem,abstract_problem,
                                 concrete_trajectories,abstract_trajectories)
  
except Exception as e:
  import traceback
  traceback.print_exc()

