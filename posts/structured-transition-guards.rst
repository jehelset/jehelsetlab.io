.. title: Structured Transition Guards 
.. slug: structured-transition-guards
.. date: 2021-01-06 07:13:15 UTC+01:00
.. type: text
.. tags: hybrid automata,deterministic finite automata,simulation
.. has_math: true

The definitions of hybrid automata that I have come across define the transition guards as subsets of the continouous state space. Here I propose a more structured definition; one suitable for computation using differential-algebraic equation system solvers with root-finding capabilities, like IDA from [Hindmarsh2005]_.

.. TEASER_END

A miniature |HA|
================

We start from the definition in [Schaft2000]_:

.. math::
   \begin{gathered}
   H = (L,X,A,W,E,Inv,Act) \\
   e = (l,a,Guard_{ll^\prime},Jump_{ll^\prime},l^\prime) \\
   Guard_{ll^\prime} \subset X
   \end{gathered}

Only *some* of these components are useful here. We throw out unneeded pieces: symbols (:math:`A`, :math:`a`), invariants (:math:`Inv`), and jumps (:math:`Jump_{ll^\prime}`). To add insult to injury we rename its activity-function (:math:`Act`) to :math:`\mathbf{F}`, and guard (:math:`Guard_{ll^\prime}`) to :math:`g`. Lets assume that the |DAEs| are regular [1]_ for good measure. A |HA| with :math:`M` discrete variables and :math:`N` continuous variables is then:

.. math::
   
   \begin{gathered}
   H^M_N = (L,X,E,\mathbf{F}) \\
   L = \Z^M \\
   X = \R^N \times \R^N \times \R\\
   E \subset L \times \mathcal{P}(X) \times L \\
   \mathbf{F} \colon L \to X \to \R^N
   \end{gathered}

Simulating |HAs| means advancing :math:`?(P,x) \in L \times X` through interleavings of continuous and discrete problems. In the continuous problem we are given an initial continuous state, an activity and a set of guards. After advancing the solution until one of the guards become active, we are rewarded with a discrete problem. In the discrete problem we are given an initial discrete state, zero or more symbols (corresponding f.ex. to an active guard), and compute the next discrete state in which a new continuous problem is waiting for us. The scope of this post streches from the start of solving a continuous problem all the way up to and including, detecting that a guard is active.

.. [1] Not overdetermined, nor underdetermined.

Logical expressions and |DFAs|
==============================

We shift our focus to the booleans, :math:`\mathbb{B} = \{ \bot, \top \}`, and expressions over boolean variables :math:`?b \in \mathbb{B}^N`. These expressions, :math:`g \colon \mathbb{B}^N \to \mathbb{B}`, involve the usual suspects; :math:`\land`, :math:`\lor`, :math:`\neg`. We intend to use these expressions to represent guards, which let us know when a continuous problem is solved. To this end we construct a |DFA| that traces changes in the truth value of :math:`g` caused by changes in :math:`?b`. We begin from the definition of a |DFA| in [Hopcroft2006]_:

.. math::

   \begin{gathered}
   D = (Q,\Sigma,\delta,q_0,F) \\
   \delta \colon Q \times \Sigma \to Q
   \end{gathered} \tag{automaton}

We ignore the initial state :math:`q_0`, and instead use a variable :math:`?q \in Q`, separate from the definition of the |DFA| (:math:`?q^0` being analogous to :math:`q_0`). We let :math:`\top_i` denote a change of :math:`?b^{t}_i = \bot` to :math:`?b^{t+1}_i = \top`, and vice versa from :math:`\bot` to :math:`\top`. These symbols define the alphabet:

.. math::
   
   \Sigma = \displaystyle \bigcup_{i \in N} \{ \top_i, \bot_i \} \tag{alphabet}

The states of the |DFA|, :math:`Q`, correspond to the elements of :math:`\mathbb{B}^N`:

.. math::
   Q = \{ q_0, \ldots, q_{2^N - 1} \} \tag{states}

:math:`q_i` is mapped to :math:`b \in \mathbb{B}^N` by representing :math:`i` in base 2 and identifying :math:`b_j` with the :math:`j`\ th bit of :math:`i` (:math:`0 \to \bot`, :math:`1 \to \top`). This mapping is denoted :math:`h`:

.. math::

   h \colon Q \to \mathbb{B}^N

The terminals are the states for whose valuations the guard evaluates to true.

.. math::
   F = \{ q \in Q \mid (g \circ h)(q) = \top \} \tag{terminals}

The transition function, :math:`\delta`, is defined in two parts. :math:`\delta_{move}` reacts to changes in valuation, while :math:`\delta_{stay}` absorb valuations already present in the current state. In what follows the specific type of change is abstracted away by :math:`\Delta_j \in \{ \top_j, \bot_j \}`. Note that the automaton is deterministic by construction:

.. math::
   \begin{aligned}
   \delta_{move}(q_i,\Delta_j) &= q_{i \oplus 2^j} \\
   \delta_{stay}(q_i,\Delta_j) &= q_i 
   \end{aligned}

.. math::
   \delta(q_i,\Delta_j) = \begin{cases}
     h(q_i)_j = \Delta \to \delta_{move}(q_i,\Delta_j) \\
     h(q_i)_j \neq \Delta \to \delta_{stay}(q_i,\Delta_j)
   \end{cases} \tag{transitions}
   
Evaluating :math:`g(b)` can be done by checking if the |DFA| accepts the symbol sequence :math:`\{ (?b_j)_j \ldots \}`. The truth-value of :math:`g` is equivalent to the terminality of the final valuation :math:`?q = q_f`. Due to the way the |DFA| is constructed it does not matter which state is chosen as :math:`?q^0`:

.. math::

   g(b) = \begin{cases} q_f \in T \to \top \\ q_f \notin T \to \bot\end{cases}

Responding to a change in the valuation of :math:`b_j`, one would simply continue from :math:`?q`, consume the corresponding symbol and see if :math:`?q` now is in an accepting state. By consuming a symbol, :math:`\Delta_j` we mean computing a new valuation of :math:`q`, :math:`?q^{t+1} = \delta(?q^{t},\Delta_j)`.

Simulating structured guards
============================

Recall that [Schaft2000]_ defined the guard as a subset of :math:`X`. We will restructure it to a form suitable for computation by defining the guard as a logical expression over a variable :math:`?b \in \mathbb{B}^{|G|}`, and identify :math:`?b` with the truth-values of implicit algebraic inequalities :math:`R = \{ r(x) \}`:

.. math::

   b_i(x) = \begin{cases}
   r_i(x) < 0 \to \top \\
   r_i(x) \nless 0 \to \bot
   \end{cases}

We abuse notation and write :math:`g(x)` for :math:`g(b_i(x) \ldots)`. The guard :math:`g \colon X \to \mathbb{B}` describes the subset :math:`\{ x \in X \mid g(x) = \top \}`, and the transitions of :math:`H^M_N` are now:

.. math::
   \begin{aligned}
   E &\subset L \times \mathbf{G} \times L \\
   \mathbf{G} &= \{ X \to \mathbb{B} \}
   \end{aligned}

Let :math:`R_i = \{ r(x) \}` denote the inequalities implicit in :math:`g_i`, and :math:`R = \bigcup_{i \in |G|} R_i` the inequalities implicit in all of :math:`G`. :math:`R` are the root-functions provided to our root-finding solver, which in turn outputs the positive and negative roots of :math:`R`, :math:`\{ +j, -j \; | \; j \in |R| \}` as the continuous problem is being solved. These roots correspond to changes in the truth values of :math:`r_j(x) < 0`, and thus to changes in :math:`?b_j`, and thus (!) to symbols in :math:`D(G)`.

We now have all the pieces to outline an algorithm for solving a continuous problem with structured guards. We construct the guard automata, and define a guard variable :math:`?q \in \prod_{i \in |G|} Q_i`, with :math:`?q^0 = \{ q_{i0} \; \mid \; i \in |G| \}`. We then compute and consume the roots implicit in the initial state, and continue feeding it the roots found during simulation. For every root :math:`\Delta_j`, :math:`q` is advanced in lockstep :math:`?q^{t+1} = \delta^\ast(?q^{t},\Delta_j)`:

.. math::

   \delta^\ast_i(q_i,\Delta_j) = \begin{cases}
   \exists r_k \in R_i \mid r_k = r_j \to \delta_i(q_i,\Delta_k) \\
   \nexists r_k \in R_i \mid r_k = r_j \to q_i
   \end{cases}

.. math::

   \delta^\ast(q,\Delta_j) = \{ \delta^\ast_i(q_i,\Delta_j) \mid i \in |G| \} 

When any :math:`q_i` is in an accepting state, it means a guard is active, and that the continuous problem has been solved. The algorithm is illustrated with a diagram:

.. graphviz::

   digraph G{
     label="Solving continuous problems with structured guards"
     splines=ortho
     node[shape=box;style=filled;fillcolor="#f0c674"]
     vb[label="Begin";shape=ellipse]
     v0[label="Construct guards\n and initialize variable"]
     v1[label="Advance solution"]
     v2[label="Root found?";shape=hexagon]
     v3[label="Advance guards"]
     {
       rank=same;
       v4[label="In accepting state?";shape=hexagon]
       ve[label="End";shape=ellipse]
     }
     v5[label="Compute initial roots"]

     vb -> v0
     v0 -> v5
     v5 -> v4
     v3 -> v4
     v1 -> v2
     v4 -> v1[taillabel="no"]
     v4 -> ve[taillabel="yes";label="    ";constraint=false]
     v2 -> v1[taillabel="no";constraint=false]
     v2 -> v3[taillabel="yes"]
   }

Example system
==============

Lest do some simple simulations. Recall that when simulating a |HA|, an active guard marks the end of a continuous problem and the beginning of a discrete one. Here we instead continue solving the continuous problem and advancing the guard automata, as the purpose is to illustrate the correlation between continuous roots and the change in state of guard |DFAs|. 

To this end we define a class of hybrid automata with sinusoid dynamics, :math:`S(A,b) \colon \R^{+^{|A|}}_{\ast} \times \R \to \mathbf{H^1_{|A|}}`:

.. math::
   \begin{gathered}
     S(A,b) = (\{0\},X,\{(0,g,0)\},F)\\
     X = \R^N \times \R^N \times \R\\
     g = \bigwedge\limits_{i = 0}^{N} ?(b - x_i < 0) \\
     F = \{ \dot{x}_i - sin(a_i \cdot t) \} 
   \end{gathered}

The system is modelled and simulated with hysj_ and IDA_ from [Hindmarsh2005]_. The illustrations are done with matplotlib_ from [Hunter2007]_. The plot shows the parameter, :math:`b`, and the continuous state, :math:`x`, drawn on top of the guard automaton for :math:`g`. :math:`\delta_{stay}` is not drawn. The active state :math:`?q` has a non-white facecolor, with terminals :math:`F` and green, and non-terminals :math:`Q \setminus F` in red. Code listing :ref:`below <code>`.
   
:math:`S(\{ 1.0 \},0.5)`:

.. raw:: html

   <video width="640" height="480" class="embed" controls>
         <source src="/files/structured-transition-guards-1.mp4" type="video/mp4">
   </video>

:math:`S(\{ 0.75, 1.25 \},0.5)`:

.. raw:: html
         
   <video width="640" height="480" class="embed" controls>
         <source src="/files/structured-transition-guards-2.mp4" type="video/mp4">
   </video>

:math:`S(\{ 0.75, 1.0, 1.25\},0.5)`:


.. raw:: html
      
   <video width="640" height="480" class="embed" controls>
         <source src="/files/structured-transition-guards-3.mp4" type="video/mp4">
   </video>

Problems
========

This approach is only appropriate for |HAs| whose guards involve few inequalities, due to the explosion of the order and size of the guard automata.

We defined the guard with respect to (strict) inequalities. A root finding solver reports roots, :math:`r(x) = 0`. A positive root would imply :math:`r(x) < 0`, but a negative root does not imply :math:`r(x) \nless 0`. The guard is not *really* active yet, and might not ever be if :math:`\dot{x} = 0`. In addition one might want to support nonstrict inequalities and equalities. 

The formulation of guards as |DFAs| suggests a defininiton of non-determinism of the |HA| in terms of the existence of a location for which the product of the guard-|DFAs| are non-deterministic. This was not pursued here.

Glossary
========

.. glossary::

   DFA
     Deterministic finite-state automaton

   DAE
     Differential-algebraic equation

   HA
     Hybrid automaton
     
.. |DFA| replace:: :term:`DFA`
.. |DFAs| replace:: :term:`DFA`\ s
.. |DAE| replace:: :term:`DAE`
.. |DAEs| replace:: :term:`DAE`\ s
.. |HA| replace:: :term:`HA`
.. |HAs| replace:: :term:`HA`\ s
        
Bibliography
============

.. [Schaft2000] "Arjan van der Schaft and Hans Schumacher", "An introduction to hybrid dynamical systems"
.. [Hunter2007] "Hunter, J. D.","Matplotlib: A 2D graphics environment"
.. [Hindmarsh2005] "Hindmarsh, Alan C and Brown, Peter N and Grant, Keith E and Lee, Steven L and Serban, Radu and Shumaker, Dan E and Woodward, Carol S","SUNDIALS: Suite of nonlinear and differential/algebraic equation solvers"
.. [Hopcroft2006] "John E  Hopcroft, Rajeev Motwani, Jeffrey D Ullman", "Introduction to Automata Theory, Languages, and Computation"

.. Links
                
.. _hysj: https://gitlab.com/jehelset/hysj
.. _matplotlib: https://matplotlib.org/
.. _IDA: https://computing.llnl.gov/projects/sundials/ida

.. _code: 

Code
====

.. include:: code/structured-transition-guards.py
   :code: python
