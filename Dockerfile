FROM registry.gitlab.com/paddy-hack/nikola

RUN apk add --no-cache \
        git            \
        graphviz
RUN pip install gitpython

