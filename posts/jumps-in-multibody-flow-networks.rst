.. title: Jumps in Multibody Flow Networks 
.. slug: jumps-in-multibody-flow-networks
.. date: 2021-01-08 00:56:14 UTC+01:00
.. type: text
.. tags: hybrid automata, flow networks, graph theory
.. has_math: true
.. status: draft

Here I define a multi-body flow network; a hybrid automaton representing zero or more flow-networks (*the bodies*), that are non-overlapping subgraphs of a larger pipe-network (*the skeleton*). Bodies are modelled as discrete state, and the flows that run through them are modelled as continuous state. Bodies grow, shrink, fuse and split as the volumes of the vertices they inhabit fill and drain. The shifting of bodies change the conservation equations that constrain the flow, and require jumps in continuous state.

.. TEASER_END
 
Barebones |HA| with jumps
=========================

We start by extending definition of the |HA| in `Lazy Hybrid Automata <../posts/lazy-hybrid-automata>`_ with jumps :math:`\mathbf{J} \colon E \to X \to X`. Jumps model instantaneous change in the continuous state of the |HA| and are here assumed to be in the form implicit algebraic equations, :math:`J(x) = 0`.

.. math::
   \begin{gathered}
   H = (L,X,\mathbf{E},\mathbf{J}) \\
   \mathbf{E} \colon L \to E \\
   \mathbf{J} \colon E \to X \to X
   \end{gathered}

If a continuous problem is solved by activating the guard of the edge :math:`(G_1,J_1,l_{1})` with :math:`?(P,x)^{t} = (l_0,x_0)`, the solution of the continuous problem at :math:`l_1` will start out at :math:`(l_{1},x_{1})` where :math:`J(x_{1}) = 0`. A jump function of :math:`\mathbf{J}(e) = J(x) = x - ?x^t`, is equivalent to having no jumps. 

Jumping sawteeth
----------------

We illustrate jumps with a class of sawtooth |HAs|, :math:`H \colon \R^3 \to \mathbb{H}^1_1`:

.. math::

   \begin{gathered}
   H(a) = (\{l_0\},\R^3,\mathbf{F},\mathbf{E}) \\
   \mathbf{F} = a_0 - \dot{x} \\
   \mathbf{E} = (x - a_1,x^+ - a_2,l_0)
   \end{gathered}

The system has only one location, and one (self) transition. The activity, the guard and the jump are all simple affine functions parametrized on :math:`a`. Given :math:`?x^0 < a_1`, and :math:`a_0 > 0`, :math:`x` will grow until the guard is activated. We follow the transition from :math:`l_0 \to l_0` and compute the jump, which gives :math:`x = a_2`. If :math:`a_2 < a_1` :math:`x` will, again, grow until the guard is activated, and so on and so forth.

Below is a plot of simulations for various instances of the sawtooth system. Jumps are marked with circular markers, and drawn with dotted lines. The guard parameter is drawn in cyan, while the jump parameter is drawn in orange. See :ref:`here <code>` for code listing.

.. raw:: html

  <img src="/files/jumps-in-multibody-flow-networks-0-0.svg" class="figure"/>
  <img src="/files/jumps-in-multibody-flow-networks-0-1.svg" class="figure"/>
  <img src="/files/jumps-in-multibody-flow-networks-0-2.svg" class="figure"/>

Skeletons, bodies, limbs
========================

Our discrete state is defined in terms of a skeleton inhabited by multiple bodies composed by limbs. The skeleton represents the pipe-network, each body represent a connected body of liquid, and each limb describes whether or not a half-pipe contains water. This skeleton is represented by a directed graph :math:`G = (V,E)`. In this post we will use :math:`G_0` as an example:

.. math::

   \begin{gathered}
   G_0 = (V_0,E_0) \\
   V_0 = \{ v_i \mid i \in 4 \}\\
   E_0 = \{ (v_0,v_2),(v_1,v_2),(v_2,v_3) \} \\
   \end{gathered}

Or, as a diagram:

.. graphviz::

   digraph{
     label=<G<sub>0</sub>>
     center=true
     rankdir=LR
     node[shape=box;height=0.4;width=1.0,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     v_0[label=<v<sub>0</sub>>,fillcolor="#fef2d6"]
     v_1[label=<v<sub>1</sub>>,fillcolor="#fef2d6"]
     v_2[label=<v<sub>2</sub>>,fillcolor="#fef2d6"]
     v_3[label=<v<sub>3</sub>>,fillcolor="#fef2d6"]

     v_0:e -> v_2:w
     v_1:e -> v_2:w
     v_2:e -> v_3:w     
   }

The bodies are represented by discrete variables. The variables are defined by splitting each vertex, :math:`v_i`, in two halves, or *limbs*, :math:`b_i = \{b_{i0},b_{i1}\}`. Each limb is either empty, :math:`\sigma`, partially-, :math:`\delta`, or fully submerged, :math:`\rho`. Surface normals of partially submerged limbs point inwards into the vertex. This variable assignement means that a vertex can be a part of at most 2 bodies at the same time. For the purposes of this post we assume that the incident limbs of a junction are either all non-empty, :math:`b_{ij} \neq \sigma`, or empty. 

.. math::
   \begin{gathered}
   b = \{ b_{ij} \; | \; i,j \in |V| \times 2 \} \tag{discrete states} \\
   b_{ij} \in L \\
   L = \{ \sigma, \delta, \rho \}
   \end{gathered}

We illustrate how :math:`b` gives rise to bodies on :math:`G` with two derived graphs. We call these graphs limb graphs, because their vertices are limbs with particular values, :math:`b_{ij} \in L^\prime`. Their edges, :math:`E^\prime` are constructed from two sets. :math:`E^\prime_0` mirrors a subset of the edges of the skeleton, :math:`G`, while :math:`E^\prime_1` tie the limbs of a single vertex togther:

.. math::

   G^\prime(b,L^\prime) = (V^\prime,E^\prime) \tag{limb graph} 

.. math::

   V^\prime = \{ b_{ij} \; | \; b_{ij} \in L^\prime \} \tag{limb vertices}

.. math::

   \begin{gathered}
   E^\prime = E^\prime_0 \cup E^\prime_1 \tag{limb edges}\\
   E^\prime_0 = \{ (b_{i_01},b_{i_10}) \in V^{\prime} \times V^\prime \; | \; \exists (v_{i_0},v_{i_1}) \in E \} \\
   E^\prime_1 = \{ (b_{i0},b_{i1}) \in V^\prime \times V^\prime \}
   \end{gathered}

The body graph, :math:`G^\rho`, is the limb graph of :math:`L^\rho = \{ \delta, \rho \}`. Its connected components are bodies.

.. math:: G^\rho = G^\prime(b,L^\rho) \tag{body graph}

Its complement, the cavity graph, :math:`G^\sigma`, is the limb graph of :math:`L^\sigma = \{ \sigma, \delta \}`. Its connected components are cavities, separating bodies.
          
.. math:: G^\sigma = G^\prime(b,L^\sigma) \tag{cavity graph}

We let :math:`Y^\star` denote the junctions of the corresponding limb-graph, :math:`Y^\star = Y(G^\star)`, where :math:`\star \in \{ \rho, \sigma \}`.

Lets inspect our example graph, :math:`G_0`, under :math:`b^- = \{ (\rho,\rho),(\rho,\rho),(\delta,\delta),(\rho,\rho) \}`. The wet parts are drawn in red, while the dry parts are drawn in yellow. Edges between two limbs of the same vertex are drawn with a dotted line.

.. graphviz::

   digraph{
     label=<b<sup>-</sup>>
     rankdir=LR
     node[shape=box;height=0.4;width=0.5,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     b_0_0[label=<b<sub>0,0</sub>>,fillcolor="#dc1717"]
     b_1_0[label=<b<sub>1,0</sub>>,fillcolor="#dc1717"]
     b_2_0[label=<b<sub>2,0</sub>>,fillcolor="#dc1717;0.5:#fef2d6"]
     b_3_0[label=<b<sub>3,0</sub>>,fillcolor="#dc1717"]
     b_0_1[label=<b<sub>0,1</sub>>,fillcolor="#dc1717"]
     b_1_1[label=<b<sub>1,1</sub>>,fillcolor="#dc1717"]
     b_2_1[label=<b<sub>2,1</sub>>,fillcolor="#fef2d6;0.5:#dc1717"]
     b_3_1[label=<b<sub>3,1</sub>>,fillcolor="#dc1717"]
     
     b_0_0:e -> b_0_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_1_0:e -> b_1_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_2_0:e -> b_2_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_3_0:e -> b_3_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]

     b_0_1:e -> b_2_0:w
     b_1_1:e -> b_2_0:w
     b_2_1:e -> b_3_0:w     
   }
   
:math:`b^-` produces two bodies and one cavity:

.. graphviz::

   digraph{
     label=<G<sup>&rho;</sup>>
     rankdir=LR
     node[shape=box;height=0.4;width=0.5,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     b_0_0[label=<b<sub>0,0</sub>>,fillcolor="#dc1717"]
     b_1_0[label=<b<sub>1,0</sub>>,fillcolor="#dc1717"]
     b_2_0[label=<b<sub>2,0</sub>>,fillcolor="#dc1717;0.5:#fef2d6"]
     b_3_0[label=<b<sub>3,0</sub>>,fillcolor="#dc1717"]
     b_0_1[label=<b<sub>0,1</sub>>,fillcolor="#dc1717"]
     b_1_1[label=<b<sub>1,1</sub>>,fillcolor="#dc1717"]
     b_2_1[label=<b<sub>2,1</sub>>,fillcolor="#fef2d6;0.5:#dc1717"]
     b_3_1[label=<b<sub>3,1</sub>>,fillcolor="#dc1717"]
     
     b_0_0:e -> b_0_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_1_0:e -> b_1_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_2_0:e -> b_2_1:w[dir=both;style=invis]
     b_3_0:e -> b_3_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]

     b_0_1:e -> b_2_0:w
     b_1_1:e -> b_2_0:w
     b_2_1:e -> b_3_0:w     
   }

.. graphviz::

   digraph{
     label=<G<sup>&sigma;</sup>>
     rankdir=LR
     node[shape=box;height=0.4;width=0.5,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     b_0_0[label=<b<sub>0,0</sub>>,style=invis]
     b_1_0[label=<b<sub>1,0</sub>>,style=invis]
     b_2_0[label=<b<sub>2,0</sub>>,fillcolor="#dc1717;0.5:#fef2d6"]
     b_3_0[label=<b<sub>3,0</sub>>,style=invis]
     b_0_1[label=<b<sub>0,1</sub>>,style=invis]
     b_1_1[label=<b<sub>1,1</sub>>,style=invis]
     b_2_1[label=<b<sub>2,1</sub>>,fillcolor="#fef2d6;0.5:#dc1717"]
     b_3_1[label=<b<sub>3,1</sub>>,style=invis]
     
     b_0_0:e -> b_0_1:w[dir=both;style=invis]
     b_1_0:e -> b_1_1:w[dir=both;style=invis]
     b_2_0:e -> b_2_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_3_0:e -> b_3_1:w[dir=both;style=invis]

     b_0_1:e -> b_2_0:w[style=invis]
     b_1_1:e -> b_2_0:w[style=invis]
     b_2_1:e -> b_3_0:w[style=invis]
   }

Flows, flows, flows
===================

With the discrete state in order, we can now set up the continuous state of our system; the flows in and out of limbs.

.. math::
   q = \{ q_{ijk} \; | \; b_{ij},k \in b \times 2 \} \tag{continuous states} 

The continuous state-space is constrained by a set of conservation equations, :math:`D_\circ(b,q) = 0`. Let :math:`q^\prime_{ijk}` denote the signed junction flow:

.. math::
   q^\prime_{ijk} = \begin{cases} k = 0& \to &-q_{ijk} \\ k = 1& \to &q_{ijk} \end{cases}

The sum of the signed flows incident to a junction in the body graph, :math:`G^\rho`, must be zero. 

.. math::
   D^Y_\circ = \left\{ \displaystyle \sum^{}_{b_{ij} \in V(y)} q^{\prime}_{ijj} \; | \; y \in Y^\rho \right. \tag{junction conservation} 
   :label: eq0

Since this flow network is inelastic, the flows in and out of submerged limbs must be equal.

.. math::
   D^\rho_\circ = \left\{ \displaystyle \sum^{}_{k \in 2} q^\prime_{ijk} \; | \; b_{ij} \in b, b_{ij} = \rho \right. \tag{submerged limb conservation}
   :label: eq1
           
The flow incident to junctions in the cavity graph, :math:`G^\sigma`, must be zero:
           
.. math::
   D^\sigma_\circ = \Big\{ q_{ijj^\prime} \; | \; b_{ij} \in V(y), y \in Y^\sigma \tag{cavity conservation}
   :label: eq2

Together these two sets of equation form :math:`D_\circ`.
   
Fusing bodies
=============

Consider the transition :math:`b^- \to \; b^+` caused by a change in the :math:`i_\ast`\ th limbs, where :math:`b^-_{i_\ast j} = \delta`.

.. math::
   b^+_{ij} = \begin{cases} i = i_\ast \to& \rho \\ i \neq i_\ast \to& b^-_{ij} \end{cases}

The transition is illustrated using :math:`G_0` and :math:`b^-` from the earlier sections, with :math:`i_\ast = 2`:

.. graphviz::

   digraph{
     label=<b<sup>-</sup>>
     rankdir=LR
     node[shape=box;height=0.4;width=0.5,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     b_0_0[label=<b<sub>0,0</sub>>,fillcolor="#dc1717"]
     b_1_0[label=<b<sub>1,0</sub>>,fillcolor="#dc1717"]
     b_2_0[label=<b<sub>2,0</sub>>,fillcolor="#dc1717;0.8:#fef2d6"]
     b_3_0[label=<b<sub>3,0</sub>>,fillcolor="#dc1717"]
     b_0_1[label=<b<sub>0,1</sub>>,fillcolor="#dc1717"]
     b_1_1[label=<b<sub>1,1</sub>>,fillcolor="#dc1717"]
     b_2_1[label=<b<sub>2,1</sub>>,fillcolor="#fef2d6;0.2:#dc1717"]
     b_3_1[label=<b<sub>3,1</sub>>,fillcolor="#dc1717"]
     
     b_0_0:e -> b_0_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_1_0:e -> b_1_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_2_0:e -> b_2_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_3_0:e -> b_3_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]

     b_0_1:e -> b_2_0:w
     b_1_1:e -> b_2_0:w
     b_2_1:e -> b_3_0:w     
   }

.. graphviz::

   digraph{
     label=<b<sup>+</sup>>
     rankdir=LR
     node[shape=box;height=0.4;width=0.5,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     b_0_0[label=<b<sub>0,0</sub>>,fillcolor="#dc1717"]
     b_1_0[label=<b<sub>1,0</sub>>,fillcolor="#dc1717"]
     b_2_0[label=<b<sub>2,0</sub>>,fillcolor="#dc1717"]
     b_3_0[label=<b<sub>3,0</sub>>,fillcolor="#dc1717"]
     b_0_1[label=<b<sub>0,1</sub>>,fillcolor="#dc1717"]
     b_1_1[label=<b<sub>1,1</sub>>,fillcolor="#dc1717"]
     b_2_1[label=<b<sub>2,1</sub>>,fillcolor="#dc1717"]
     b_3_1[label=<b<sub>3,1</sub>>,fillcolor="#dc1717"]
     
     b_0_0:e -> b_0_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_1_0:e -> b_1_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_2_0:e -> b_2_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]
     b_3_0:e -> b_3_1:w[dir=both;style=dotted;arrowhead=box;arrowtail=box]

     b_0_1:e -> b_2_0:w
     b_1_1:e -> b_2_0:w
     b_2_1:e -> b_3_0:w     
   }
   
The change in :math:`?b` will change the conservation equations. :math:`D(b^-,q^-) = 0`, but :math:`D(b^+,b^-) \neq 0`, so :math:`(b^+,q^-)` is not a valid hybrid state. Specifically, since :math:`i_\ast` is full submerged in :math:`b^+`, surrounded by body-junctions on both sides, the flows in and out of its limbs must, according to equation :eq:`eq0` be equal:

.. math::
   \begin{gathered}
   q_{i_{\ast}00} = q_{i_{\ast}01} \\
   q_{i_{\ast}01} = q_{i_{\ast}10} \\
   q_{i_{\ast}10} = q_{i_{\ast}11}
   \end{gathered}
          
But in :math:`b^-,q^-` the volumes of :math:`i_\ast` had to be02 filling, thus: 

.. math:: q_{i_{\ast}00} - q_{i_{\ast}11} > 0

In addition both its limbs were partially submerged, forming a cavity with the edge :math:`(b_{i_\ast,0},b_{i_\ast,1})`, and by :eq:`eq1`:

.. math::
   \begin{gathered}
   q_{i_{\ast}01} = 0 \\
   q_{i_{\ast}10} = 0
   \end{gathered}

We need to find a :math:`q^+`, that satisfies :math:`D(b^+,b^+) = 0`. We define :math:`Q^+` in terms of a displacement :math:`Q^\Delta`, with :math:`Q^+ = Q^- + Q^\Delta`. :math:`Q^\Delta` is a solution of a set of equations :math:`D(B^+,Q^\Delta) = 0`, termed the displacement equations.

Let :math:`\Delta = q^-_{i_{\ast}00} - q^-_{i_{\ast}11}` denote the difference in flow in and out of :math:`i_\ast`, define :math:`q^+_{i_{\ast}jj} = q^-_{i_{\ast}jj} + \Delta_j`, where :math:`\Delta = \sum_{j \in 2} \Delta_j`. And set up the local displacement equations:

.. math::
   D_{\Delta} = \Big\{\begin{array}{c}
   q^\Delta_{i_{\ast}jj} - \Delta_j \\
   q^\Delta_{i_{\ast}jj} - (q^-_{i_{\ast}jj^\prime} + \Delta_j) \end{array} \quad j \in 2 \tag{local displacement}

The first equation displaces the flow in and out of the vertex, which ensures :math:`q^+_{i_{\ast00}} - q^+_{i_{\ast11}} = 0`, the second ensures conservation inside, and between the limbs. This change will in turn violate junction conservation (:eq:`e0`) in :math:`y^j(b_{i_{\ast}j})` whose net flows are now :math:`\Delta_j`. This is countered by distributing this local displacement among the other flows of the junctions incident limbs. 

.. math::
   D_{\nabla} = \{ q^{\Delta\prime}_{i_0j_0j_0} - q^{\Delta\prime}_{i_1j_1j_1} \; | \; (b_{i_0j_0},b_{i_1j_1}) \in V(y), y \in Y^{\rho}(?B^-) \tag{distribution}


The displacement equations :math:`D` are then:

.. math::
   D = \left\{ D_{\Delta}, D_{\nabla},D_{\circ} \right\} \tag{displacement}
   
   
And :math:`Q^\Delta` found, by solving :math:`D(B^+,Q^\Delta) = 0`.


Glossary
========

.. glossary::

   HA
     Hybrid automaton
     
.. |HA| replace:: :term:`HA`
.. |HAs| replace:: :term:`HA`\ s

Bibliography
============

.. [Schaft2000] "Arjan van der Schaft and Hans Schumacher", "An introduction to hybrid dynamical systems"


.. _code: 

Code
====

.. include:: code/jumps-in-multibody-flow-networks-0.py
   :code: python

                
