.. title: Remodeling a Flower System
.. slug: remodeling-a-flower-system
.. date: 2021-02-24 03:55:14 UTC+01:00
.. type: text
.. tags: hybrid automata, piecewise affine systems
.. has_math: true

Here we reformulate the piecewise affine flower system referred to in [Hedlund99]_ as a `Lazy Hybrid Automaton <../lazy-hybrid-automata>`_ and simulate it as such with hysj_.

.. TEASER_END

Remodeling
===========
   
The |PWA| flower system is restated here\ [1]_ as:

.. math::
   \dot{x}(t) = \begin{cases} A_0x(t), \quad x^2_0(t) - x^2_1(t) \geq 0 \\ A_1x(t), \quad x^2_0(t) - x^2_1(t) < 0\end{cases}

.. math::
   A_0 = \begin{bmatrix} \epsilon & \alpha \omega \\ -\omega & -\epsilon \end{bmatrix}

.. math::
   A_1 = \begin{bmatrix} -\epsilon & \omega \\ -\alpha\omega & -\epsilon \end{bmatrix}

Which is remodeled as the |HA|\ [2]_:

.. math::

   F_i = A_i x

.. math::

   G_{ii^\prime} = x^2_i - x^2_{i^\prime} 
   
.. math::

   E_i = \{ l_{i^\prime} \}

The constraints on the guards of the |HA| are here only that they are expressible as logical expressions over functions whose roots can be detected by the solver used for simulation\ [3]_. The difference in squares is here used directly, but one could also f.ex. have formulated the guard :math:`x^2_{i^\prime} - x^2_i < 0` as :math:`(x_i > x_{i^\prime}) \land (-x_i < x_{i^\prime})`.

.. [1] Using null-indexing.
.. [2] Using subscripts :math:`F_i` instead of :math:`F(l_i)`.
.. [3] See `Structured Transition Guards <../structured-transition-guards>`_.

Simulating
==========

The |HA| is simulated in python with hysj_ and IDA_, using the original model parameters\ [4]_\ [5]_. The simulation is illustrated with matplotlib_ and numpy_.  The rays used to define the regions of the state-space in [Hedlund99]_ are added for illustrative purposes. Roots are drawn with dots. The phase plane of the currently active location is drawn in the background. Code listing :ref:`below <code>`.

.. raw:: html
      
   <video width="640" height="480" class="embed" controls>
         <source src="/files/remodeling-a-flower-system.mp4" type="video/mp4">
   </video>
   
.. [4] Ignoring time management.
.. [5] The bootstrapping process outlined in `Structured Transition Guards <../structured-transition-guards>`_ ensures that one does not need to specify the correct initial discrete location since the |HA| is deterministic in the sense of Kowalewski et al. in [Lunze09]_.

Glossary
========

.. glossary::

   PWA
     Piecewise affine

   HA
     Hybrid automaton
     
.. |PWA| replace:: :term:`PWA`
.. |HA| replace:: :term:`HA`

   
Bibliography
============

.. [Hedlund99] "Sven Hedlund", "Computational Methods for Hybrid Systems"
.. [Hindmarsh2005] "Hindmarsh, Alan C and Brown, Peter N and Grant, Keith E and Lee, Steven L and Serban, Radu and Shumaker, Dan E and Woodward, Carol S","SUNDIALS: Suite of nonlinear and differential/algebraic equation solvers"
.. [Lunze09] "Jan Lunze", "Handbook of Hybrid Systems Control"

.. Links

.. _hysj: https://gitlab.com/jehelset/hysj
.. _IDA: https://computing.llnl.gov/projects/sundials/ida
.. _matplotlib: https://matplotlib.org/
.. _numpy: https://numpy.org/

.. _code: 

Code
====

.. include:: code/remodeling-a-flower-system.py
   :code: python
