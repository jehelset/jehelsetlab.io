.. title: Notation 
.. slug: notation
.. date: 2021-01-07 06:10:21 UTC+01:00
.. type: text
.. has_math: true

Potentially idiosyncratic notation used throughout the rest of the pages.

Sets
====

When the integer :math:`X` appears in a context where a set is expected, it is taken to mean :math:`\{
0, \ldots, X-1 \}`. F.ex. :math:`A \times 2 = A \times \{0,1\}`.

When an index :math:`i \in \{ 0, 1 \}` appears primed, :math:`i^\prime` it is taken to mean :math:`i`\ 's complement in :math:`\{ 0, 1 \}`. 

Variables
=========

A variable :math:`x` inhabits some set :math:`X`. Its valuation is denoted :math:`?x`. The valuation is assumed to change in discrete steps, and the trace of the variable is the sequence :math:`\{ ?x^{t} \ldots \}`,with :math:`t \in \N`. If the superscript is omitted :math:`?x` typically refers to the latest valuation of the trace. :math:`?x \in X` is taken to mean a variable :math:`x` in :math:`X`.

If a variable :math:`?p \in P`, is a tuple, :math:`?(x,y) \in X \times Y` the same notation is used on the components of the tuple, ie :math:`?p^t`, :math:`?(x,y)^t` and :math:`(?x^t,?y^t)` all convey the same meaning.

Higher order functions
======================

A higher order function is a function, :math:`f \colon A \to G`, where :math:`g \in G` are themselves functions, :math:`g \colon B \to C`. It is denoted here with :math:`f \colon A \to B \to C`, ie :math:`\to` is right-associative in this context.


Graphs
======

The junction of a directed graph, :math:`G = (V,E)`, is a set of incident edges, :math:`y \subseteq E`. Two edges, :math:`e_0 = (v_{00},v_{01})`, :math:`e_1 = (v_{10},v_{11})`, are said to be incident if source or target is equal, ie :math:`(v_{00} = v_{10} \lor v_{01} = v_{11})`. The junctions, :math:`Y = \{ y \ldots \}`, are found by taking the transitive closure of this incidence relation on :math:`E`. Note that :math:`\bigcup \limits_{y \in Y} = E` and :math:`y_0 \neq y_1 \implies y_0 \cap y_1 = \emptyset`.

:math:`V(y)` denotes vertices incident to :math:`e \in y`, :math:`V^0(y)` denotes sources in :math:`e \in y`, and :math:`V^1(y)` denotes targets. :math:`y^0(v)` denotes the junction containing the edges whose target is :math:`v`, and :math:`y^1(v)` denotes the junction containing the edges whose source is :math:`v`.

Lets clarify with an example, the graph :math:`G_0`:

.. math::

   \begin{gathered}
   G_0 = (V_0,E_0) \\
   V_0 = \{ v_i \mid i \in 4 \}\\
   E_0 = \{ (v_0,v_2),(v_1,v_2),(v_2,v_3) \} \\
   \end{gathered}

Or, as a diagram:

.. graphviz::

   digraph{
     label=<G<sub>0</sub>>
     center=true
     rankdir=LR
     node[shape=box;height=0.4;width=1.0,style=striped,fillcolor="#fef2d6"]
     edge[arrowsize=0.5]
     v_0[label=<v<sub>0</sub>>,fillcolor="#fef2d6"]
     v_1[label=<v<sub>1</sub>>,fillcolor="#fef2d6"]
     v_2[label=<v<sub>2</sub>>,fillcolor="#fef2d6"]
     v_3[label=<v<sub>3</sub>>,fillcolor="#fef2d6"]

     v_0:e -> v_2:w
     v_1:e -> v_2:w
     v_2:e -> v_3:w     
   }

Here are some sample results for :math:`G_0` using the definitions above:

.. math::
   \begin{aligned}
   Y &= \{ \{e_0,e_1 \}, \{e_2\} \} \\
   V^0(y_0) &= \{ v_0, v_2 \} \\
   V^1(y_1) &= \{ v_3 \} \\
   y^0(v_2) &= y_0 \\ 
   y^0(v_0) &= \emptyset
   \end{aligned}
   
