((nil
  (eval
   .
   (if
       (not (boundp 'nikola/loaded))
       (progn
         (setq projectile-project-configure-cmd "rm -rf __pycache__ cache public .doit.db")
         (setq projectile-project-compilation-cmd "nikola build")
         (setq projectile-project-run-cmd "nikola auto -b")
         (setq nikola/dir (projectile-project-root))
         (setq nikola/loaded 1))))))
